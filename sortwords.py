#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """

    first = first.lower()
    second = second.lower()
    if len(first) > len(second):
        next = first
        second = first
        next = second
        for i in range (len(first)):
            lower = False
            if first [i] < second [i]:
                lower = True
                break
            return lower

def get_lower(words: list, pos: int):
    """Get lower word, for words right of pos (including pos)"""

    lower = pos
    for j in range (pos, len(words)):
        if is_lower(words[pos],words[j]) == False:
            pos = j
    return lower

def sort(words: list):
    """Return the list of words, ordered alphabetically"""

    listed = []
    for k in range (0, len(words)):
        number = get_lower(words, k)
        listed += [words[number]]
        words[number] = words [k]
    return listed

def show(words: list):
    """Show words on screen, using print()"""

    return print(words)


def main():
    words: list = sys.argv[1:]
    ordered: list = sort(words)
    show(ordered)


if __name__ == '__main__':
    main()
